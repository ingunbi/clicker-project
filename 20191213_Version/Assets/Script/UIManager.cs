﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    public Text goldDisplayer; //Text 변수 선언 주의
    public Text goldPerClickDisplayer;
    public DataController dataController;

    void Update()
    {
        goldDisplayer.text = "Gold " + dataController.GetGold();
        goldPerClickDisplayer.text = "Gold Per Click " + dataController.GetGoldPerClick();
    }
}
