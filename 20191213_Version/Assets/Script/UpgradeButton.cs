﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UpgradeButton : MonoBehaviour
{
    public DataController dataController;

    public Text upgradeDisplayer;
    public string upgradeName;

    public int goldByUpgrade;
    public int startGoldByUpgrade = 1;
    public int currentCost;   //item cost
    public int startCurrentCost = 1;
    public int level = 1;  //level

    public float upgradePow = 2.07f;  //upgrade 이후 클릭당 얻는 비용
    public float costPow = 3.14f; //upgrade 가격

    void Start()
    {
        dataController.LoadUpgradeButton(this);
        UpdateUI();

    }

    public void PurchaseUpgrade()
    {
        if (dataController.GetGold() >= currentCost)
        {
            dataController.SubGold(currentCost);
            level = level + 1;
            dataController.AddGoldPerClick(goldByUpgrade);

            UpdateUpgrade();
            UpdateUI();
            dataController.SaveUpgradeButton(this);
        }
    }

    public void UpdateUpgrade()
    {
        goldByUpgrade = startGoldByUpgrade * (int)Mathf.Pow(upgradePow, level);
        currentCost = startCurrentCost * (int)Mathf.Pow(costPow, level);
    }

    public void UpdateUI()
    {
        upgradeDisplayer.text = upgradeName + "\nCost: " + currentCost + "\nLevel" + level + "\nAdd GoldPerClick: " + goldByUpgrade;

    }
}
