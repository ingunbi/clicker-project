﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DataController : MonoBehaviour
{
    private int m_gold = 0;   //DataController의 변수를 외부에서 쉽게 바꾸지 못하도록 private 변수로 선언
    private int m_goldPerClick = 0;

    //게임시작시 데이터는 Load, 이벤트 마다 데이터 Save를 한다.

    //Data Load
    void Awake()
    {
        //PlayerPrefs.DeleteAll();
        m_gold = PlayerPrefs.GetInt("Gold");
        m_goldPerClick = PlayerPrefs.GetInt("GoldPerClick", 1);
    }


    //Data Save
    public void SetGold(int newGold)   //골드 Data Set 함수
    {
        m_gold = newGold;
        PlayerPrefs.SetInt("Gold", m_gold);
    }

    public void AddGold(int newGold)   //골드 증가 함수
    {
        m_gold = m_gold + newGold;
        //m_gold += newgold;
        SetGold(m_gold);
    }

    public void SubGold(int newGold)   //골드 감소 함수
    {
        m_gold = m_gold - newGold;
        //m_gold -= newgold;
        SetGold(m_gold);
    }

    public int GetGold() //골드 값 반환
    {
        return m_gold;
    }

    public int GetGoldPerClick()  //클릭당 골드값 반환
    {
        return m_goldPerClick;
    }

    public void SetGoldPerClick(int newGoldPerClick) //클릭당 골드 Data Set 함수 void, int 주의하기
    {
        m_goldPerClick = newGoldPerClick;
        PlayerPrefs.SetInt("GoldPerClick", m_goldPerClick);
    }

    // 클릭당 골드
    public void AddGoldPerClick(int newGoldPerClick)
    {
        m_goldPerClick = m_goldPerClick + newGoldPerClick;
        SetGoldPerClick(m_goldPerClick);
    }

    public void LoadUpgradeButton(UpgradeButton upgradeButton)
    {
        string key = upgradeButton.upgradeName;
        upgradeButton.level = PlayerPrefs.GetInt(key + "_level", 1);
        upgradeButton.goldByUpgrade = PlayerPrefs.GetInt(key + "_goldByUpgrade", upgradeButton.startGoldByUpgrade);
        upgradeButton.currentCost = PlayerPrefs.GetInt(key + "_cost", upgradeButton.startCurrentCost);

    }

    public void SaveUpgradeButton(UpgradeButton upgradeButton)
    {
        string key = upgradeButton.upgradeName;
        PlayerPrefs.SetInt(key + "_level", upgradeButton.level);
        PlayerPrefs.SetInt(key + "_goldByUpgrade", upgradeButton.goldByUpgrade);
        PlayerPrefs.SetInt(key + "_cost", upgradeButton.currentCost);
    }

    // 초당 골드
    public void LoadItemButton(ItemButton itemButton)
    {
        string key = itemButton.itemName;

        itemButton.level = PlayerPrefs.GetInt(key + "_level");
        itemButton.currentCost = PlayerPrefs.GetInt(key + "_cost", itemButton.startCurrentCost);
        itemButton.goldPerSec = PlayerPrefs.GetInt(key + "_goldPerSec");

        if (PlayerPrefs.GetInt(key + "_isPurchased") == 1)
        {
            itemButton.isPurchased = true;
        }
        else
        {
            itemButton.isPurchased = false;
        }
    }

    public void SaveItemButton(ItemButton itemButton)
    {
        string key = itemButton.itemName;

        PlayerPrefs.SetInt(key + "_level", itemButton.level);
        PlayerPrefs.SetInt(key + "_cost", itemButton.currentCost);
        PlayerPrefs.SetInt(key + "_goldPerSec", itemButton.goldPerSec);

        if (itemButton.isPurchased == true)
        {
            PlayerPrefs.SetInt(key + "_isPurchased", 1); //구매 하면 1로 Set
        }
        else
        {
            PlayerPrefs.SetInt(key + "_isPurchased", 0); //구매 하지 않으면 0으로 Set
        }
    }

}
