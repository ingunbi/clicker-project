﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ItemButton : MonoBehaviour
{
    public DataController dataController;

    public Text itemDisplayer;
    public string itemName;
    public int level;
    public int currentCost;
    public int startCurrentCost = 1;
    public int goldPerSec;
    public int startGoldPerSec = 1;
    public float costPow = 3.14f;
    public float upgradePow = 1.07f;
    public bool isPurchased = false;

    void Start()
    {
        dataController.LoadItemButton(this);
        StartCoroutine("AddGoldLoop");
        UpdateUI();
    }

    public void PurchaseItem()  //아이템 구매
    {
        if(dataController.GetGold() >=currentCost)
        {
            isPurchased = true;
            dataController.SubGold(currentCost);
            level = level + 1;

            UpdateItem();
            UpdateUI();

            dataController.SaveItemButton(this);
        }
    }

    IEnumerator AddGoldLoop()  //초당 골드 획득
    {
        while(true)
        {
            if(isPurchased)
            {
                dataController.AddGold(goldPerSec);
            }
            yield return new WaitForSeconds(1.0f);
        }
    }

    public void UpdateItem()  //아이템 정보 업데이트
    {
        goldPerSec = goldPerSec + startGoldPerSec * (int)Mathf.Pow(upgradePow, level); //초당 획득 골드량 업데이트
        currentCost = startGoldPerSec * (int)Mathf.Pow(costPow, level);  //아이템 레벨업 비용
    }

    public void UpdateUI()
    {
        itemDisplayer.text = itemName + "\nLevel: " + level + "\ncost: " + currentCost + "\nGold Per Sec: " + goldPerSec + "\nIsPurchased: " + isPurchased;
    }

}
